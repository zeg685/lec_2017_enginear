#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <zbar.h>
#include <iostream>
#include <iomanip>


using namespace std;
using namespace cv;
using namespace zbar;

void CaptureWEB ()
{

	//Data Structure to store cam.
	CvCapture* cap=cvCaptureFromCAM(1);
	//Image variable to store frame
	IplImage* frame;
	//Window to show livefeed
	cvNamedWindow("LiveFeed",CV_WINDOW_AUTOSIZE);
	if(!cap)
	{
		cout << "ERROR: Capture is null!\n";
	}
	while(1)
	{
		//Load the next frame
		frame=cvQueryFrame(cap);
		//If frame is not loaded break from the loop
		if(!frame)
			break;
		//Show the present frame
		cvShowImage("LiveFeed",frame);
		//Escape Sequence
		char c=cvWaitKey(33);
		//If the key pressed by user is Esc(ASCII is 27) then break out of the loop
		if(c==27)
		{
			IplImage *img=cvQueryFrame(cap);
			cvSaveImage("kappa.jpeg",img);
			waitKey(1000);
			img=cvQueryFrame(cap);
			cvSaveImage("kappa2.jpeg",img);
			waitKey(1000);
			img=cvQueryFrame(cap);
			cvSaveImage("kappa3.jpeg",img);
			break;
		}
	}
	//CleanUp
	cvReleaseCapture(&cap);
	cvDestroyAllWindows(); 

}

int getBarcode()
{

	// Create a zbar reader
	ImageScanner scanner;

	// Configure the reader
	scanner.set_config(ZBAR_NONE, ZBAR_CFG_ENABLE, 1);


	// Get the imageto scan
	Mat frame, frame_grayscale;
	frame = imread("kappa.jpeg", CV_LOAD_IMAGE_COLOR);

	// Convert to grayscale
	cvtColor(frame,frame_grayscale, CV_BGR2GRAY);

	// Obtain image data
	int width = frame_grayscale.cols;
	int height = frame_grayscale.rows;
	uchar *raw = (uchar *)(frame_grayscale.data);

	// Wrap image data
	Image image(width, height, "Y800", raw, width * height);

	// Scan the image for barcodes
	//int n = scanner.scan(image);
	scanner.scan(image);

	// Extract results
	int counter = 0,k=0;
	for (Image::SymbolIterator symbol = image.symbol_begin(); symbol != image.symbol_end(); ++symbol) 
	{
		k=1;
		time_t now;
		tm *current;
		now = time(0);
		current = localtime(&now);

		// do something useful with results
		cout    << "[" << current->tm_hour << ":" << current->tm_min << ":" << setw(2) << setfill('0') << current->tm_sec << "] " << counter << " "
			<< "decoded " << symbol->get_type_name()
			<< " symbol \"" << symbol->get_data() << '"' << endl;

		//cout << "Location: (" << symbol->get_location_x(0) << "," << symbol->get_location_y(0) << ")" << endl;
		//cout << "Size: " << symbol->get_location_size() << endl;

		// Draw location of the symbols found
		if (symbol->get_location_size() == 4) {
			//scoate rectangle-ul daca nu iti place ce face
			for(int m=0;m<4;++m)                
			{	
				rectangle(frame, Rect(symbol->get_location_x(m), symbol->get_location_y(m), 10, 10), Scalar(0, 255, 0));
			}
			line(frame, Point(symbol->get_location_x(0), symbol->get_location_y(0)), Point(symbol->get_location_x(1), symbol->get_location_y(1)), Scalar(0, 255, 0), 2, 8, 0);
			line(frame, Point(symbol->get_location_x(1), symbol->get_location_y(1)), Point(symbol->get_location_x(2), symbol->get_location_y(2)), Scalar(0, 255, 0), 2, 8, 0);
			line(frame, Point(symbol->get_location_x(2), symbol->get_location_y(2)), Point(symbol->get_location_x(3), symbol->get_location_y(3)), Scalar(0, 255, 0), 2, 8, 0);
			line(frame, Point(symbol->get_location_x(3), symbol->get_location_y(3)), Point(symbol->get_location_x(0), symbol->get_location_y(0)), Scalar(0, 255, 0), 2, 8, 0);
		}

		// Get points
		/*for (Symbol::PointIterator point = symbol.point_begin(); point != symbol.point_end(); ++point) {
		  cout << point << endl;
		  } */
		counter++;
	}

	if(!k)
	{
		cout<<"Barcode not found"<<endl;
	}

	// Show captured frame, now with overlays!
	//imshow("captured", frame);

	// clean up
	image.set_data(NULL, 0);
	return k;

}

int main()
{

	int k=0;
	do
	{
		cout<<"Apasa ESC cand esti gata"<<endl;
		CaptureWEB();
		k=getBarcode();
	}while(k==0);
	return 0;
}



