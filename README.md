EngiNEAR


Our project consists in 4 parts. First part is getting the camera streams on the board and save a frame to a jpeg file. 
The second part is scanning that jpeg for a ISBN barcode and showing the output. 
We skipped the third part because we had problems (using the Goodreads API). 
And the fourth part consists in merging the previous parts together.

For the hardware part we have a Microsoft HD Life 300 to get 720p stream from it and a Udoo Neo Extended board to program on.
For the software part we used OpenCV and Zbar, all open-source.

The source code of the final project is in the file final.cpp

To use this applicaton, you have to write in a terminal ./final and follow the steps.

Press ESC when you focused the camera on a barcode, and the scanned barcode will be displayed on the terminal window.